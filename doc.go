/*
Package sqltx contains simplified closure based transaction support to work with standard `sql` package.
*/
package sqltx
