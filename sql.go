package sqltx

import (
	"context"
	"database/sql"
	"log"

	"github.com/pkg/errors"
)

type contextKey string

var (
	transactionKey      contextKey = "transactionKey"
	transactionDepthKey contextKey = "transactionDepthKey"
)

type SqlTransaction struct {
	db *sql.DB
}

func NewSqlTransaction(db *sql.DB) *SqlTransaction {
	return &SqlTransaction{db: db}
}

/*
InTransaction creates and starts a new Session and use it to call the fn callback.
The Context contains `sql.Tx` instance and should be used for any operations in the fn callback that supposed to be
executed under the session. To retrieve the `pgx.Tx` instance from ctx, use `FromContext` function.
If the ctx parameter already contains a Session, that Session will be reused instead effectively become 1 transaction.
To create new transaction instead of reused transaction, supply different ctx which doesn't contains any `sql.Tx`.
Any error returned by the fn callback will abort (rollback) the transaction, otherwise commit it.
Original error will be returned if commit/rollback successful, otherwise will wrap the original error under commit/rollback
error.
The provided TxOptions is optional and may be zeroed if defaults should be used.
*/
func (s *SqlTransaction) InTransaction(ctx context.Context, fn func(context.Context) error, opts *sql.TxOptions) error {
	var commitErr error
	var txCtx context.Context

	tx := FromContext(ctx)
	depth := transactionDepthFromContext(ctx) + 1

	if tx == nil {
		var err error
		tx, err = s.db.BeginTx(ctx, opts)
		if err != nil {
			return err
		}

		txCtx = context.WithValue(ctx, transactionKey, tx)
	} else {
		txCtx = ctx
	}

	txCtx = context.WithValue(txCtx, transactionDepthKey, depth)

	panicked := true
	defer func() {
		// Make sure to rollback when panic, or Commit error
		if panicked || commitErr != nil {
			err := tx.Rollback()

			if err != nil && err != sql.ErrTxDone {
				log.Print(errors.WithMessagef(err, "failed to rollback transaction: panicked: %v, commitErr: %v", panicked, commitErr))
			}
		}
	}()

	err := fn(txCtx)

	// if error then should rollback no matter how depth
	if err != nil {
		rollbackErr := tx.Rollback()
		if rollbackErr != nil {
			err = errors.Wrap(err, rollbackErr.Error())
		}

		return err
	}

	// if no error then check if depth = 1, commit only when depth=1 (the most outer transactions)
	if depth == 1 {
		commitErr = tx.Commit()
		panicked = false

		return commitErr
	}

	panicked = false
	return nil
}

// FromContext return pgx.Tx instance (if any) from the specified context. Return nil if not found.
func FromContext(ctx context.Context) *sql.Tx {
	if tx, ok := ctx.Value(transactionKey).(*sql.Tx); ok {
		return tx
	}
	return nil
}

func transactionDepthFromContext(ctx context.Context) int {
	if depth, ok := ctx.Value(transactionDepthKey).(int); ok {
		return depth
	}
	return 0
}
