package sqltx_test

import (
	"context"
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
	"gitlab.com/exa-bit/pkg/sql-tx"
)

func try(err error) {
	if err != nil {
		panic(err)
	}
}

var (
	postTable    = "posts"
	commentTable = "comments"
)

func dropSqlTestTables(db sqlx.Execer) {
	_, _ = db.Exec(`DROP TABLE ` + commentTable)
	_, _ = db.Exec(`DROP TABLE ` + postTable)
}

func createSqlTestTables(db sqlx.Execer) {
	_, err := db.Exec(`CREATE TABLE ` + postTable + `(
		id integer not null primary key auto_increment,
    	title varchar(250) not null
	)`)
	try(err)
	_, err = db.Exec(`CREATE TABLE ` + commentTable + `(
		id integer not null primary key auto_increment,
		post_id integer not null references posts(id),
		review varchar(250) not null
	)`)
	try(err)
}

// drop tables to clean it up, then recreate the database and collections
func prepareSqlTestEnvironment(db sqlx.Execer) {
	dropSqlTestTables(db)
	createSqlTestTables(db)
}

type post struct {
	ID    int
	Title string
}

type comment struct {
	ID     int
	PostID int
	Review string
}

type execer interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
}

func insertPost(db execer, p *post) {
	stmt := `INSERT INTO ` + postTable + `(title) VALUES (?)`
	res, err := db.Exec(stmt, p.Title)
	try(err)
	lastId, err := res.LastInsertId()
	try(err)
	p.ID = int(lastId)
}

func insertComment(db execer, c *comment) {
	stmt := `INSERT INTO ` + commentTable + `(post_id, review) VALUES (?, ?)`
	res, err := db.Exec(stmt, c.PostID, c.Review)
	try(err)
	lastId, err := res.LastInsertId()
	try(err)
	c.ID = int(lastId)
}

// test atomic commit multiple tables
func TestSqlInTransaction_AtomicCommit(t *testing.T) {
	db, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(db)
		db.Close()
	}()

	prepareSqlTestEnvironment(db)

	txManager := sqltx.NewSqlTransaction(db)
	var postId int
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId = p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		return nil
	}, nil)
	if assert.NoError(t, err) {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM "+postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 1)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM "+commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 1)
		assert.Equal(t, comments[0].PostID, postId)
	}
}

// test atomic rollback multiple tables
func TestSqlInTransaction_AtomicRollback(t *testing.T) {
	db, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(db)
		db.Close()
	}()

	prepareSqlTestEnvironment(db)

	txManager := sqltx.NewSqlTransaction(db)
	var postId int
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId = p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		return errors.New("should rollback")
	}, nil)
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM " + postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}
		assert.Len(t, posts, 0)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM " + commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 0)
	}
}

// test two nested transactions, the inner session should committed while the outer session should rollback
func TestSqlInTransaction_NestedSession(t *testing.T) {
	db, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(db)
		db.Close()
	}()

	prepareSqlTestEnvironment(db)

	txManager := sqltx.NewSqlTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		err = txManager.InTransaction(context.TODO(), func(ctx2 context.Context) error {
			txc2 := sqltx.FromContext(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertComment(txc2, cmt)

			return nil
		}, nil)
		assert.NoError(t, err)

		return errors.New("should rollback")
	}, nil)
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM " + postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 1)
		assert.Equal(t, "My post 2", posts[0].Title)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM " + commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 1)
		assert.Equal(t, "My review 2", comments[0].Review)
	}
}

// test two nested transactions with reused ctx which should execute operations in one transaction. This test should
// commit all operations.
func TestSqlInTransaction_NestedSession_Reused_AtomicCommit(t *testing.T) {
	db, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(db)
		db.Close()
	}()

	prepareSqlTestEnvironment(db)

	txManager := sqltx.NewSqlTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		err = txManager.InTransaction(ctx, func(ctx2 context.Context) error {
			txc2 := sqltx.FromContext(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertComment(txc2, cmt)

			return nil
		}, nil)
		assert.NoError(t, err)

		return nil
	}, nil)
	if assert.NoError(t, err) {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM " + postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 2)
		assert.Equal(t, "My post", posts[0].Title)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM " + commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 2)
		assert.Equal(t, "My review", comments[0].Review)
	}
}

// test two nested transactions with reused ctx which should execute operations in one transaction. This test should
// rollback all operations.
func TestSqlInTransaction_NestedSession_Reused_AtomicRollback(t *testing.T) {
	db, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(db)
		db.Close()
	}()

	prepareSqlTestEnvironment(db)

	txManager := sqltx.NewSqlTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		err = txManager.InTransaction(ctx, func(ctx2 context.Context) error {
			txc2 := sqltx.FromContext(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertComment(txc2, cmt)

			return nil
		}, nil)
		assert.NoError(t, err)

		return errors.New("should rollback")
	}, nil)
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM " + postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 0)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM " + commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 0)
	}
}
