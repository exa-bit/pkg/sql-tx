package sqltx_test

import (
	"context"
	"database/sql"
	"gitlab.com/exa-bit/pkg/sql-tx"
	"log"
)

func ExampleSqlInTransaction() {
	var db *sql.DB // assume db is valid

	txManager := sqltx.NewSqlTransaction(db)
	err := txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContext(ctx)
		p := &post{
			Title: "My post",
		}
		stmt := `INSERT INTO ` + postTable + `(title) VALUES ($1) RETURNING id`
		var postId int
		err := txc.QueryRowContext(context.TODO(), stmt, p.Title).Scan(&postId)
		if err != nil {
			return err
		}

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		stmt = `INSERT INTO ` + commentTable + `(post_id, review) VALUES ($1, $2) RETURNING id`
		_, err = txc.ExecContext(context.TODO(), stmt, cmt.PostID, cmt.Review)
		if err != nil {
			return err // return error to rollback transaction
		}
		return nil // return nil (no error) will commit transaction
	}, nil)
	log.Fatal("cannot start transaction!", err)
}
