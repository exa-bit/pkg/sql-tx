# exa-bit/pkg/sql-tx

Contains simplified closure based transaction support to work with standard `sql` package.
The aim is to provide similar function signature to work with sql transaction as with another database (ie. mongodb and pgx).

## Tested On
- MySql 8.0.20 - `github.com/go-sql-driver/mysql v1.5.0`

