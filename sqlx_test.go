package sqltx_test

import (
	"context"
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
	"gitlab.com/exa-bit/pkg/sql-tx"
)


// test atomic commit multiple tables
func TestSqlxInTransaction_AtomicCommit(t *testing.T) {
	stddb, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(stddb)
		stddb.Close()
	}()
	prepareSqlTestEnvironment(stddb)

	db := sqlx.NewDb(stddb, "mysql")
	txManager := sqltx.NewSqlxTransaction(db)
	var postId int
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContextX(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId = p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		return nil
	}, nil)
	if assert.NoError(t, err) {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM "+postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 1)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM "+commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 1)
		assert.Equal(t, comments[0].PostID, postId)
	}
}

// test atomic rollback multiple tables
func TestSqlxInTransaction_AtomicRollback(t *testing.T) {
	stddb, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(stddb)
		stddb.Close()
	}()
	prepareSqlTestEnvironment(stddb)

	db := sqlx.NewDb(stddb, "mysql")
	txManager := sqltx.NewSqlxTransaction(db)
	var postId int
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContextX(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId = p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		return errors.New("should rollback")
	}, nil)
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM " + postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}
		assert.Len(t, posts, 0)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM " + commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 0)
	}
}

// test two nested transactions, the inner session should committed while the outer session should rollback
func TestSqlxInTransaction_NestedSession(t *testing.T) {
	stddb, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(stddb)
		stddb.Close()
	}()
	prepareSqlTestEnvironment(stddb)

	db := sqlx.NewDb(stddb, "mysql")
	txManager := sqltx.NewSqlxTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContextX(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		err = txManager.InTransaction(context.TODO(), func(ctx2 context.Context) error {
			txc2 := sqltx.FromContextX(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertComment(txc2, cmt)

			return nil
		}, nil)
		assert.NoError(t, err)

		return errors.New("should rollback")
	}, nil)
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM " + postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 1)
		assert.Equal(t, "My post 2", posts[0].Title)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM " + commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 1)
		assert.Equal(t, "My review 2", comments[0].Review)
	}
}

// test two nested transactions with reused ctx which should execute operations in one transaction. This test should
// commit all operations.
func TestSqlxInTransaction_NestedSession_Reused_AtomicCommit(t *testing.T) {
	stddb, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(stddb)
		stddb.Close()
	}()
	prepareSqlTestEnvironment(stddb)

	db := sqlx.NewDb(stddb, "mysql")
	txManager := sqltx.NewSqlxTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContextX(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		err = txManager.InTransaction(ctx, func(ctx2 context.Context) error {
			txc2 := sqltx.FromContextX(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertComment(txc2, cmt)

			return nil
		}, nil)
		assert.NoError(t, err)

		return nil
	}, nil)
	if assert.NoError(t, err) {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM " + postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 2)
		assert.Equal(t, "My post", posts[0].Title)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM " + commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 2)
		assert.Equal(t, "My review", comments[0].Review)
	}
}

// test two nested transactions with reused ctx which should execute operations in one transaction. This test should
// rollback all operations.
func TestSqlxInTransaction_NestedSession_Reused_AtomicRollback(t *testing.T) {
	stddb, err := sql.Open("mysql", os.Getenv("MYSQL_URI"))
	if err != nil {
		panic(err)
	}
	defer func() {
		dropSqlTestTables(stddb)
		stddb.Close()
	}()
	prepareSqlTestEnvironment(stddb)

	db := sqlx.NewDb(stddb, "mysql")
	txManager := sqltx.NewSqlxTransaction(db)
	err = txManager.InTransaction(context.TODO(), func(ctx context.Context) error {
		txc := sqltx.FromContextX(ctx)
		p := &post{
			Title: "My post",
		}
		insertPost(txc, p)
		postId := p.ID

		cmt := &comment{
			PostID: postId,
			Review: "My review",
		}
		insertComment(txc, cmt)

		err = txManager.InTransaction(ctx, func(ctx2 context.Context) error {
			txc2 := sqltx.FromContextX(ctx2)
			p := &post{
				Title: "My post 2",
			}
			insertPost(txc2, p)
			postId2 := p.ID

			cmt := &comment{
				PostID: postId2,
				Review: "My review 2",
			}
			insertComment(txc2, cmt)

			return nil
		}, nil)
		assert.NoError(t, err)

		return errors.New("should rollback")
	}, nil)
	if assert.EqualError(t, err, "should rollback") {
		var posts = make([]*post, 0)
		rows, err := db.Query("SELECT id, title FROM " + postTable)
		try(err)
		for rows.Next() {
			var p = new(post)
			try(rows.Scan(&p.ID, &p.Title))
			posts = append(posts, p)
		}

		assert.Len(t, posts, 0)

		var comments = make([]*comment, 0)
		rows, err = db.Query("SELECT id, post_id, review FROM " + commentTable)
		try(err)
		for rows.Next() {
			var cmt = new(comment)
			try(rows.Scan(&cmt.ID, &cmt.PostID, &cmt.Review))
			comments = append(comments, cmt)
		}
		assert.Len(t, comments, 0)
	}
}
