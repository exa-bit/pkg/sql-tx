package sqltx

import (
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"log"

	"github.com/pkg/errors"
)

var (
	transactionKeyX      contextKey = "transactionKeyX"
	transactionDepthKeyX contextKey = "transactionDepthKeyX"
)

type SqlxTransaction struct {
	db *sqlx.DB
}

func NewSqlxTransaction(db *sqlx.DB) *SqlxTransaction {
	return &SqlxTransaction{db: db}
}

/*
InTransaction creates and starts a new Session and use it to call the fn callback.
The Context contains `sqlx.Tx` instance and should be used for any operations in the fn callback that supposed to be
executed under the session. To retrieve the `pgx.Tx` instance from ctx, use `FromContext` function.
If the ctx parameter already contains a Session, that Session will be reused instead effectively become 1 transaction.
To create new transaction instead of reused transaction, supply different ctx which doesn't contains any `sqlx.Tx`.
Any error returned by the fn callback will abort (rollback) the transaction, otherwise commit it.
Original error will be returned if commit/rollback successful, otherwise will wrap the original error under commit/rollback
error.
The provided TxOptions is optional and may be zeroed if defaults should be used.
*/
func (s *SqlxTransaction) InTransaction(ctx context.Context, fn func(context.Context) error, opts *sql.TxOptions) error {
	var commitErr error
	var txCtx context.Context

	tx := FromContextX(ctx)
	depth := transactionDepthFromContextX(ctx) + 1

	if tx == nil {
		var err error
		tx, err = s.db.BeginTxx(ctx, opts)
		if err != nil {
			return err
		}

		txCtx = context.WithValue(ctx, transactionKeyX, tx)
	} else {
		txCtx = ctx
	}

	txCtx = context.WithValue(txCtx, transactionDepthKeyX, depth)

	panicked := true
	defer func() {
		// Make sure to rollback when panic, or Commit error
		if panicked || commitErr != nil {
			err := tx.Rollback()

			if err != nil && err != sql.ErrTxDone {
				log.Print(errors.WithMessagef(err, "failed to rollback transaction: panicked: %v, commitErr: %v", panicked, commitErr))
			}
		}
	}()

	err := fn(txCtx)

	// if error then should rollback no matter how depth
	if err != nil {
		rollbackErr := tx.Rollback()
		if rollbackErr != nil {
			err = errors.Wrap(err, rollbackErr.Error())
		}

		return err
	}

	// if no error then check if depth = 1, commit only when depth=1 (the most outer transactions)
	if depth == 1 {
		commitErr = tx.Commit()
		panicked = false

		return commitErr
	}

	panicked = false
	return nil
}

// FromContext return pgx.Tx instance (if any) from the specified context. Return nil if not found.
func FromContextX(ctx context.Context) *sqlx.Tx {
	if tx, ok := ctx.Value(transactionKeyX).(*sqlx.Tx); ok {
		return tx
	}
	return nil
}

func transactionDepthFromContextX(ctx context.Context) int {
	if depth, ok := ctx.Value(transactionDepthKeyX).(int); ok {
		return depth
	}
	return 0
}
